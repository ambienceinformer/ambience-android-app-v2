package com.ambienceinformer.ambienceinformer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class AppSettingTab extends Fragment
{
    EditText did;
    Button set;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView=inflater.inflate(R.layout.app_setting_tab,container,false);

        set=(Button)rootView.findViewById(R.id.bt_set_device);
        did=(EditText)rootView.findViewById(R.id.et_set_device_id);

        return rootView;
    }
}
