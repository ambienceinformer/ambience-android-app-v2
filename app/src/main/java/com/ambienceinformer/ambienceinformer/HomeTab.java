package com.ambienceinformer.ambienceinformer;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class HomeTab extends Fragment
{
    ImageButton mrefresh;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState)
    {
        rootView=inflater.inflate(R.layout.home_tab,container,false);

        //creating objects of views
        TextView mdeviceId=(TextView) rootView.findViewById(R.id.tv_device_id);
        TextView mlocation=(TextView)rootView.findViewById(R.id.tv_location);
        TextView mtemperature=(TextView) rootView.findViewById(R.id.tv_temperature);
        TextView mhumidity=(TextView) rootView.findViewById(R.id.tv_humidity);
        TextView mairPressure=(TextView) rootView.findViewById(R.id.tv_air_pressure);
        TextView mrainStatus=(TextView) rootView.findViewById(R.id.tv_rain_status);
        TextView mbattery=(TextView) rootView.findViewById(R.id.tv_battery);

        //setting data in objects
        mdeviceId.setText(MainActivity.deviceId);
        mlocation.setText(MainActivity.location);
        mtemperature.setText(MainActivity.temperature);
        mhumidity.setText(MainActivity.humidity);
        mairPressure.setText(MainActivity.airPressure);
        mrainStatus.setText(MainActivity.rainStatus);
        mbattery.setText(MainActivity.battery);

        Typeface mfontFace = Typeface.createFromAsset(getActivity().getAssets(),
                "poiretone.ttf");
        mtemperature.setTypeface(mfontFace);
        return rootView;
    }
    public void setdevicedata(String str){
        TextView mdeviceId=(TextView) rootView.findViewById(R.id.tv_device_id);
        mdeviceId.setText("str");
    }
    @Override
    public void onResume() {
        super.onResume();
        System.out.println("onResume gets called");
    }

}
