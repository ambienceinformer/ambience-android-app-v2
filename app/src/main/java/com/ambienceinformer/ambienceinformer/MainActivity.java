package com.ambienceinformer.ambienceinformer;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    public static String deviceId="N/A";
    public static String temperature="N/A";
    public static String location="N/A";
    public static String humidity="N/A";
    public static String airPressure="N/A";
    public static String rainStatus="N/A";
    public static String battery="N/A";
    static int currentTheme=0;
    ConstraintLayout mmain;
    SharedPreferences mSettings;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    static Context context;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=MainActivity.this;
        makeSystemUITransparent();
        mmain=(ConstraintLayout)findViewById(R.id.root_layout);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        //creating object of tablayout to access tabs
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        getCachedData();

    }

    //method to set device for app
    public void setDeviceMethod(View v)
    {
        Toast.makeText(MainActivity.this,"Setting",Toast.LENGTH_SHORT).show();
        EditText et=(EditText)findViewById(R.id.et_set_device_id);
        String devId=et.getText().toString();
        new SetQueryThread().execute(devId);
    }

    public void refreshButtonPressed(View rv)
    {
        if(context!=null) {
            Toast.makeText(context, "refreshing", Toast.LENGTH_SHORT).show();
            new RefreshQueryThread().execute(deviceId);
        }
    }
    public void quickChangeTheme(View v)
    {
        currentTheme=(currentTheme+1)%20;

        mSettings = getSharedPreferences("ambience2",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("theme", Integer.toString(currentTheme));
        editor.commit();
        setSelectedTheme(currentTheme);
    }

    protected void setSelectedTheme(int themeNo)
    {
        switch (themeNo)
        {
            case 0:
            {
                mmain.setBackgroundResource(R.drawable.bg1);
                break;
            }
            case 1:
            {
                mmain.setBackgroundResource(R.drawable.bg2);
                break;
            }
            case 2:
            {
                mmain.setBackgroundResource(R.drawable.bg3);
                break;
            }
            case 3:
            {
                mmain.setBackgroundResource(R.drawable.bg4);
                break;
            }
            case 4:
            {
                mmain.setBackgroundResource(R.drawable.bg5);
                break;
            }
            case 5:
            {
                mmain.setBackgroundResource(R.drawable.bg6);
                break;
            }
            case 6:
            {
                mmain.setBackgroundResource(R.drawable.bg7);
                break;
            }
            case 7:
            {
                mmain.setBackgroundResource(R.drawable.bg8);
                break;
            }
            case 8:
            {
                mmain.setBackgroundResource(R.drawable.bg9);
                break;
            }
            case 9:
            {
                mmain.setBackgroundResource(R.drawable.bg10);
                break;
            }
            case 10:
            {
                mmain.setBackgroundResource(R.drawable.bg11);
                break;
            }
            case 11:
            {
                mmain.setBackgroundResource(R.drawable.bg12);
                break;
            }
            case 12:
            {
                mmain.setBackgroundResource(R.drawable.bg13);
                break;
            }
            case 13:
            {
                mmain.setBackgroundResource(R.drawable.bg14);
                break;
            }
            case 14:
            {
                mmain.setBackgroundResource(R.drawable.bg15);
                break;
            }
            case 15:
            {
                mmain.setBackgroundResource(R.drawable.bg16);
                break;
            }
            case 16:
            {
                mmain.setBackgroundResource(R.drawable.bg17);
                break;
            }
            case 17:
            {
                mmain.setBackgroundResource(R.drawable.bg18);
                break;
            }
            case 18:
            {
                mmain.setBackgroundResource(R.drawable.bg19);
                break;
            }
            case 19:
            {
                mmain.setBackgroundResource(R.drawable.bg20);
                break;
            }

            default:
            {
                mmain.setBackgroundResource(R.drawable.bg1);
                break;
            }
        }
    }

    //function to get data from sharedPreference
    protected void saveCachedData()
    {
        mSettings = getSharedPreferences("ambience2",
                Context.MODE_PRIVATE);//msetting is the object of SharedPreference
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString("id", deviceId);
        editor.putString("theme", Integer.toString(currentTheme));
        editor.commit();
    }

    //function to get user settings from android's cache memory
    protected void getCachedData()
    {
        mSettings = getSharedPreferences("ambience2",
                Context.MODE_PRIVATE);
        if(mSettings.contains("theme"))//theme is key
        {
            currentTheme=Integer.parseInt(mSettings.getString("theme", null));
            setSelectedTheme(currentTheme);
        }
        if (mSettings.contains("id"))//id is key
        {
            deviceId = mSettings.getString("id", null);
            new RefreshQueryThread().execute(deviceId);
        }
    }

    // to make the status bar transparent
    private void makeSystemUITransparent()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    protected void parseJson(String res)
    {
        try {
            JSONObject myobj = new JSONObject(res);
            if((myobj.getString("status").equals("found")))
            {
                deviceId=myobj.getString("deviceId").toString();
                location=myobj.getString("location").toString();
                temperature=myobj.getString("temperature").toString();
                humidity=myobj.getString("humidity").toString();
                airPressure=myobj.getString("airPressure").toString();
                rainStatus=myobj.getString("rainStatus").toString();
                battery=myobj.getString("battery").toString();
                showToast("Device is set");
            }
            else if((myobj.getString("status").equals("not_found")))
            {
                showToast("No devices found");
            }
            else
            {
                showToast("No devices found");
            }
        }
        catch (JSONException je)
        {
            je.printStackTrace();
        }
    }

    protected void parseJson(String res,int i)
    {
        try {
            JSONObject myobj = new JSONObject(res);
            if((myobj.getString("status").equals("found")))
            {
                deviceId=myobj.getString("deviceId").toString();
                location=myobj.getString("location").toString();
                temperature=myobj.getString("temperature").toString();
                humidity=myobj.getString("humidity").toString();
                airPressure=myobj.getString("airPressure").toString();
                rainStatus=myobj.getString("rainStatus").toString();
                battery=myobj.getString("battery").toString();
                showToast("Refreshed");
                displayRefreshedData();
            }
            else if((myobj.getString("status").equals("not_found")))
            {
                showToast("No devices found");
            }
            else
            {
                showToast("No devices found");
            }
        }
        catch (JSONException je)
        {
            je.printStackTrace();
        }
    }

    public void displayRefreshedData()
    {
        TextView mdeviceId=(TextView)findViewById(R.id.tv_device_id);
        TextView mlocation=(TextView)findViewById(R.id.tv_location);
        TextView mtemperature=(TextView)findViewById(R.id.tv_temperature);
        TextView mhumidity=(TextView)findViewById(R.id.tv_humidity);
        TextView mairPressure=(TextView)findViewById(R.id.tv_air_pressure);
        TextView mrainStatus=(TextView)findViewById(R.id.tv_rain_status);
        TextView mbattery=(TextView)findViewById(R.id.tv_battery);

        //setting data in objects
        mdeviceId.setText(deviceId);
        mlocation.setText(location);
        mtemperature.setText(temperature);
        mhumidity.setText(humidity);
        mairPressure.setText(airPressure);
        mrainStatus.setText(rainStatus);
        mbattery.setText(battery);
    }
    public void showToast(String msg)
    {
        Toast.makeText(MainActivity.this,msg,Toast.LENGTH_SHORT).show();
    }
    public class SetQueryThread extends AsyncTask<String,Void,String>
    {

        final protected String AMBIENCE_BASE_URL="http://ambienceinformer.cf/php/JSONData.php";
        final protected String QUERY_PARAM="id";
        @Override
        protected void onPreExecute()
        {
            //mloading.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String...searches)
        {
            String searchId=searches[0];
            return getDataFromServer(searchId);
        }

        @Override
        protected void onPostExecute(String res)
        {
            //mloading.setVisibility(View.INVISIBLE);
            parseJson(res);
            saveCachedData();
            //Toast.makeText(MainActivity.this,"completed",Toast.LENGTH_SHORT).show();
        }

        protected String getDataFromServer(String id)
        {
            HttpURLConnection urlConnection=null;
            String result="";
            try {
                //URL url = new URL("http://ambience.dx.am/JSONdata.php?id=" + searchUrl);
                Uri makeUri= Uri.parse(AMBIENCE_BASE_URL).buildUpon().appendQueryParameter(QUERY_PARAM,id).build();
                URL url=new URL(makeUri.toString());
                urlConnection=(HttpURLConnection)url.openConnection();

                InputStream in=urlConnection.getInputStream();
                Scanner scanner=new Scanner(in);
                while(scanner.hasNext()) {
                    result = result+scanner.next();
                }
                return result;
            }
            catch (MalformedURLException me)
            {
                me.printStackTrace();
            }
            catch (IOException ie)
            {
                ie.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
                return result;
            }
        }
    }
    public class RefreshQueryThread extends AsyncTask<String,Void,String>
    {

        final protected String AMBIENCE_BASE_URL="http://ambienceinformer.cf/php/JSONData.php";
        final protected String QUERY_PARAM="id";
        @Override
        protected void onPreExecute()
        {
            //mloading.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String...searches)
        {
            String searchId=searches[0];
            return getDataFromServer(searchId);
        }

        @Override
        protected void onPostExecute(String res)
        {
            //mloading.setVisibility(View.INVISIBLE);
            parseJson(res,1);
            saveCachedData();
            //Toast.makeText(MainActivity.this,"completed",Toast.LENGTH_SHORT).show();
        }

        protected String getDataFromServer(String id)
        {
            HttpURLConnection urlConnection=null;
            String result="";
            try {
                //URL url = new URL("http://ambience.dx.am/JSONdata.php?id=" + searchUrl);
                Uri makeUri= Uri.parse(AMBIENCE_BASE_URL).buildUpon().appendQueryParameter(QUERY_PARAM,id).build();
                URL url=new URL(makeUri.toString());
                urlConnection=(HttpURLConnection)url.openConnection();

                InputStream in=urlConnection.getInputStream();
                Scanner scanner=new Scanner(in);
                while(scanner.hasNext()) {
                    result = result+scanner.next();
                }
                return result;
            }
            catch (MalformedURLException me)
            {
                me.printStackTrace();
            }
            catch (IOException ie)
            {
                ie.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
                return result;
            }
        }
    }
}
