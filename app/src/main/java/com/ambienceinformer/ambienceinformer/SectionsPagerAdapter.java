package com.ambienceinformer.ambienceinformer;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
            {
                HomeTab mhomeTab=new HomeTab();
                return mhomeTab;
            }
            case 1:
            {
                AppSettingTab mappSettingTab=new AppSettingTab();
                return mappSettingTab;
            }
            case 2:
            {

                DeviceSettingTab mdeviceSettingTab=new DeviceSettingTab();
                return mdeviceSettingTab;
            }

            default:
            {
                return null;
            }
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Home";
            case 1:
                return "App Setting";
            case 2:
                return "Device Setting";
        }
        return null;
    }
}